import pandas as pd

class Dataset:
    def __init__(self, data=None):
        if data is None:
            self.data = []
        else:
            self.data = list(data)
    def __len__(self):
        return len(self.data)
    def __getitem__(self, i):
        return self.data[i]
    def __getattr__(self, name):
        if (len(self) > 0 and isinstance(self.data[0], pd.core.frame.DataFrame) and
            name in self.data[0]):
            return Dataset([x[name] for x in self.data])
        def fun(*args, **kwargs):
            return Dataset([getattr(x, name)(*args, **kwargs) for x in self.data])
        return fun
    def __str__(self):
        l = []
        l.append(f'The dataset has {len(self)} elements:' + '\n')
        for i in range(1, len(self) + 1):
            l.append(f'-> {self[i-1].size} elements with shape {self[i-1].shape} and type {type(self[i-1])}')
            if i != len(self):
                l.append('\n')
        return ''.join(l)
