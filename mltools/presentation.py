from matplotlib.backends.backend_pdf import PdfPages

class Presentation:

    def __init__(self, path):
        self.file = PdfPages(path)

    def add_fig(self, fig):
        self.file.savefig(fig)

    def close(self):
        self.file.close()
    
