from ..config import options as opt
import joblib

def read_model():
    pipeline = joblib.load(opt.models_directory + 'pipeline.pkl')
    return pipeline
