import numpy as np
import pandas as pd

def profile(x, y, data=None, bins=None, use='mean', return_counts=False,):
    """

    """
    if data is None:
        df = pd.DataFrame(np.array((x, y)).T, columns = ['x', 'y'], copy=False)
    else:
        df = pd.DataFrame()
        
    if bins is None:
        _, bins = np.histogram(df['x'], 'auto')
    else:
        df = df.loc[(df['x'] >= bins.min()) & (df['x'] < bins.max())]
        bins = np.array(bins)

    bins_middle = (bins[1 : ] + bins[ : -1]) / 2

    # Last bin is increased by a small number so that the
    # last point is also included 
    bins[-1] += 0.01 * abs(bins[-1])

    dig = np.digitize(df['x'], bins)
    bins = bins_middle[dig-1]
    grouped = df.groupby(bins) 
    
    counts = grouped.size()
    x = counts.index.values
    if use == 'mean':
        y = grouped['y'].mean().values
        y_delta = grouped['y'].sem(ddof=0).values
    elif use == 'std':
        y = grouped['y'].std().values
        y_delta = np.full_like(y, 0)
    else:
        raise ValueError("The argument provided for use has to be 'mean' or 'std'")
    ans = (x, y, y_delta)

    if return_counts:
        counts = counts.values
        ans = (x, y, y_delta, counts)

    return ans
