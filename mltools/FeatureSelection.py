import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
# sns.set(style='white')
# sns.set(color_codes=True)

class FeatureSelection:
    def __init__(self, df, comparison_var):
        self.mask = {'Id': 'No', 'MSSubClass': 'No', 'MSZoning': 'Yes', 'LotFrontage': 'Yes', 'LotArea': 'Yes', 'Street': 'No', 'Alley': 'No', 'LotShape': 'Yes', 'LandContour': 'Yes', 'Utilities': 'No', 'LotConfig': 'Yes', 'LandSlope': 'No', 'Neighborhood': 'Yes', 'Condition1': 'No', 'Condition2': 'No', 'BldgType': 'No', 'HouseStyle': 'Yes', 'OverallQual': 'Yes', 'OverallCond': 'No', 'YearBuilt': 'Yes', 'YearRemodAdd': 'Yes', 'RoofStyle': 'No', 'RoofMatl': 'No', 'Exterior1st': 'Yes', 'Exterior2nd': 'Yes'}

        self.length = len(df)
        self.make_mask(df, comparison_var)

    def make_mask(self, df, comparison_var):
        df[comparison_var] = np.log(df[comparison_var])
        var_ls = []
        length = len(df)
        mask = self.mask
        for var in (var for var in df if var != comparison_var and var not in self.mask):
            var = 'SaleCondition'
            var_type = df[var].dtype
            print(f"Going with variable '{var}' with type {var_type}")
            nans = df[var].isnull().sum()
            uniq_vals = df[var].unique()
            uniq_length = len(uniq_vals)

            if nans > 0.6 * length:
                print(f"Too many nans for this variable: {nans}/{length}")
                mask[var] = 'No'
                print(mask)
                continue

            if var_type != np.object:
                fig, ax = self.make_scatter_plot(df, var, comparison_var, nans=nans)
                fig.show()
                inp = input('Keep variable? N/y')
                if inp == '' or inp.lower() == 'n':
                    mask[var] ='No'
                    print(mask)
                    continue
                elif inp.lower() == 'y':
                    mask[var] = 'Yes'
                    print(mask)
                    continue
                
            else:
                fig, ax = plt.subplots(1, 1)
                bins = np.arange(10.2, 14, 0.1)
                for value in uniq_vals:
                    if pd.isnull(value): continue
                    ax.hist(df[comparison_var][df[var] == value], bins=bins, label=value, histtype='step')
                ax.legend(fontsize='x-small')
                ax.text(0.8, 0.8, f'NaN={nans}/{length}', 
                        transform=ax.transAxes)
                fig.show()

                inp = input()
                if inp == '' or inp.lower() == 'n':
                    mask[var] ='No'
                    print(mask)
                    continue
                elif inp.lower() == 'y':
                    mask[var] = 'Yes'
                    print(mask)
                    continue
                
            # if uniq_length < 3:
            #     fig, ax = self.make_distribution_plots(df, var, comparison_var)
            #     fig.show()
            #     input()
        return var_ls

    def load_mask(self):
        try:
            df = pd.read_hdf('mask.hdf')
            return df.values
        except OverflowError:
            pass

    def make_scatter_plot(self, df, var, comparison_var, *args, **kwargs):
        with plt.rc_context({'figure.dpi': 150}):
            joint_grid = sns.jointplot(x=var, y=comparison_var, data=df, kind='reg')
            fig = joint_grid.fig
            ax = joint_grid.ax_joint
            if 'nans' in kwargs:
                ax.text(0.8, 0.8, f'NaN={kwargs["nans"]}/{self.length}', 
                        transform=ax.transAxes)
            return fig, ax 
    def make_distribution_plots(self, df, var, comparison_var):
        fig, ax = plt.subplots(1, 1)
        print(df[var])
        sns.distplot(df[var], ax=ax)
        fig.show()
        input()


# FeatureSelection(pd.read_csv('./train.csv'), 'SalePrice')
