import pandas as pd

def csv_to_hdf(name, output_name=None):
    df = pd.read_csv(name)
    if output_name is None:
        output_name = name[:name.find('.csv')] + '.hdf'
    df.to_hdf(output_name, key='data', mode='w')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Transform csv files to hdf files')
    parser.add_argument('files', type=str, nargs='*',
                         help='csv files that will be converted to hdf')
    parser.add_argument('-o', dest='output', type=str, help='name of the output')
    args = parser.parse_args()
    if args.files is None:
        parser.error('files not specified')
    for path in args.files:
        csv_to_hdf(path, args.output)
