# coding: future_fstrings
import pandas as pd
import numpy as np
import math

zenith_bins = np.arange(1, 2.01, 0.05)
energy_bins = np.arange(17.5, 18.6, 0.1)

def sample_df_by_group(df, frac, group_var):
    """
    Sample a DataFrame using a fraction of groups of rows 
    :param df: DataFrame
    :param frac: float between 0 and 1
    :param group_var: string with the variable or list of variables used
    for making the groups in `df`
    
    :return: DataFrame
    """
    if not 0 < frac < 1:
        raise ValueError("frac has to be between 0 and 1")
    if not isinstance(group_var, list):
        group_var = [group_var]
    grouped = df.groupby(group_var)
    keys = list(grouped.groups.keys())
    np.random.shuffle(keys)
    keys = keys[:int(frac * len(keys))]
    return df[df[group_var].isin(keys)]

def train_and_test_using_distr(df, dic, pre_cuts=None):
    """
    Split a dataframe into a training and test dataset
    """
    if pre_cuts is not None:
        mask = np.ones(len(df))
        for cut in pre_cuts:
            mask &= cut(df)
        df = df[mask]

    # if not using_infill_dataset:
    #     df = df[df['energy_mc'] < 20]
    #     energy_bins = np.arange(18.5, 20.01, 0.1)
    # else:
    #     df = df[df['energy_mc'] < 18.8]
    #     energy_bins = np.arange(17.5, 18.51, 0.1)
    ndf = df[['energy_mc', 'zenith_mc', 'sim_id']]
    grouped = ndf.groupby('sim_id', as_index=False)
    statistics = pd.DataFrame()
    event_df = grouped.head(1)
    event_df['weight'] = pd.cut(ndf['zenith_mc'], bins=zenith_bins, labels=False)
    event_df.replace({'weight': dic}, inplace=True)
    statistics['All events'] = event_df.groupby(np.digitize(event_df['energy_mc'], energy_bins)).size()
    print("Going into sample")
    sample = event_df.groupby(np.digitize(
        event_df['energy_mc'], energy_bins)).apply(
        lambda x: x.sample(n=500 if x.iat[0, 0] < 20 else 0,
                      weights=x['weight'], replace=False))
    print("Out of sample")

    mask = ndf['sim_id'].isin(sample['sim_id'])
    train, test = df[~mask], df[mask]
    # Sample train dataframe to reduce size
    train = train.sample(frac=0.7)

    
    # tmp = train[train['zenith_mc'] < 1.45]
    # statistics["Train:sec < 1.45"] = tmp.groupby(np.digitize(tmp['energy_mc'], energy_bins)).size()
    # tmp = train[train['zenith_mc'] > 1.45]
    # statistics["Train:sec > 1.45"] = tmp.groupby(np.digitize(tmp['energy_mc'], energy_bins)).size()
    # tmp = test[test['zenith_mc'] < 1.45]
    # statistics["Test:sec < 1.45"] = tmp.groupby(np.digitize(tmp['energy_mc'], energy_bins)).size()
    # tmp = test[test['zenith_mc'] > 1.45]
    # statistics["Test:sec > 1.45"] = tmp.groupby(np.digitize(tmp['energy_mc'], energy_bins)).size()
    # print(statistics)
    return train, test

# trace_columns = ['t' + str(i) for i in range(time_bins)]
# trace_muon_columns = ['tm' + str(i) for i in range(time_bins)]
# zenith_bins = np.arange(1, 2.01, 0.05)

def read_from_dataset():
    global energy_var, zenith_var, using_infill_dataset
    dataset_names = [
        ['../../Data/Cafpe/qgsjet-proton-cafpe.hdf',
        '../../Data/Napoli/qgsjet-proton-napoli.hdf'],
        ['../../Data/Cafpe/qgsjet-helium-cafpe.hdf',
            '../../Data/Napoli/qgsjet-helium-napoli.hdf'],
        ['../../Data/Cafpe/qgsjet-nitrogen-cafpe.hdf',
            '../../Data/Napoli/qgsjet-oxygen-napoli.hdf'],
        ['../../Data/Cafpe/qgsjet-iron-cafpe.hdf',
            '../../Data/Napoli/qgsjet-iron-napoli.hdf'],
        ]
    dataset_names = [
        ['../../Data/Ctodero/qgsjet-proton-ctodero.hdf'],
        ['../../Data/Ctodero/qgsjet-helium-ctodero.hdf'],
        ['../../Data/Ctodero/qgsjet-nitrogen-ctodero.hdf'],
        ['../../Data/Ctodero/qgsjet-iron-ctodero.hdf'],
        ]

    # dataset_names = [
    #     ['../../Data/Data/auger.hdf'],
    #     ]

    # # Ctodero-infill
    # dataset_names = [
    #     ['../../Data/Infill-ctodero/qgsjet-proton-infill-ctodero.hdf'],
    #     ['../../Data/Infill-ctodero/qgsjet-helium-infill-ctodero.hdf'],
    #     ['../../Data/Infill-ctodero/qgsjet-nitrogen-infill-ctodero.hdf'],
    #     ['../../Data/Infill-ctodero/qgsjet-iron-infill-ctodero.hdf'],
    #     ]

    # # Data-infill
    # dataset_names = [
    #     ['../../Data/Data-infill/auger-infill.hdf'],
    #     ]

    using_infill_dataset = any(('infill' in elem for elem in dataset_names[0]))
    using_auger_dataset = any(('auger' in elem for elem in dataset_names[0]))

    dataset = []
    for ls in dataset_names:
        tmp = []
        tmp.append(pd.concat([pd.read_hdf(name) for name in ls], copy=False))
        df = tmp[0]
        df.drop(columns_to_drop_safely, axis=1, errors='ignore', inplace=True)
        try:
            energy_var
        except NameError:
            energy_var = 'energy_mc' if 'energy_mc' in df else 'energy'
            zenith_var = 'zenith_mc' if 'zenith_mc' in df else 'zenith'
        trace = pd.concat([pd.read_hdf(name[:-4] + '-trace.hdf') for name in
                           ls], copy=False)
        trace.columns = trace_columns
        trace_start = len(tmp[0].columns)
        trace_end = trace_start + len(trace.columns)
        tmp.append(trace)
        try:
            trace_muon = pd.concat([pd.read_hdf(name[:-4] + '-trace-muon.hdf') for name in ls])
            trace_muon.columns = trace_muon_columns
            trace_muon_start = trace_end
            trace_muon_end = trace_muon_start + len(trace_muon.columns)
            tmp.append(trace_muon)
        except OSError:
            pass

        if not using_infill_dataset:
            mask = (df['total_signal'] > 5) & (df['saturated'] == 0) &\
                    (df[zenith_var] < math.radians(60)) & (df['r'] != 0) &\
                    (df[energy_var] > 10**18.5)
                    
            if not using_auger_dataset:
                mask &= ~((1 / np.cos(df[zenith_var]) > 1.45) & (df[energy_var] > 10**20))
        else:
            mask = (df['total_signal'] > 5) & (df['saturated'] == 0) &\
                    (df[zenith_var] < math.radians(60)) & (df['r'] != 0) &\
                    (df[energy_var] > 10**17.5) & (df[energy_var] < 10**18.8)

        for i in range(len(tmp)):
            tmp[i] = tmp[i][mask]
        dataset.append(pd.concat(tmp, axis=1, copy=False))
    return dataset

def pick_distr():
    from pathlib import Path
    home = str(Path.home())
    data_df = pd.read_hdf(home +'/Auger/'+ 'Data/Data-infill/auger-trace-test.hdf')
    # data_df = pd.read_hdf('../../Data/Data-infill/auger-trace-test.hdf')
    dic = {}
    grouped = data_df.groupby('sim_id')
    hist, _ = np.histogram(grouped['zenith'].head(1).values, zenith_bins, density=True)
    dic = dict([(i, hist[i]) for i in range(len(hist))])
    return dic

def make_train_and_test_dataset():
    for i, df in enumerate(read_from_dataset()): 
        name = {0: 'proton', 1: 'helium', 2:'nitrogen', 3:'iron'}[i]
        df[energy_var] = np.log10(df[energy_var])
        df[zenith_var] = 1 / np.cos(df[zenith_var])

        # Ctodero
        train, test = train_and_test(pick_distr(), df)
        train.to_hdf(f'../../Data/Ctodero/qgsjet-{name}-ctodero-trace-train.hdf',
                     key='data', mode='w')
        test.to_hdf(f'../../Data/Ctodero/qgsjet-{name}-ctodero-trace-test.hdf',
                    key='data', mode='w')

        # Data
        # train, test = pd.DataFrame(), df
        # test.to_hdf(f'../../Data/Data/auger-trace-test.hdf', key='data', mode='w')

        # Ctodero-infill
        # train, test = train_and_test(pick_distr(), df)
        # train.to_hdf(f'../../Data/Infill-ctodero//qgsjet-{name}-infill-ctodero-trace-train.hdf',
        #              key='data', mode='w')
        # test.to_hdf(f'../../Data/Infill-ctodero//qgsjet-{name}-infill-ctodero-trace-test.hdf',
        #             key='data', mode='w')

        # Data-infill

        # train, test = pd.DataFrame(), df
        # test.to_hdf(f'../../Data/Data-infill/auger-trace-test.hdf', key='data', mode='w')
