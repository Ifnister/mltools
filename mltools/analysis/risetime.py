# coding: future_fstrings
import pandas as pd
import numpy as np
from .defs import SEP

def set_rod(dataset):
    new_dataset = [[]] * len(dataset)
    for i, df in enumerate(dataset):
        print("Setting risetime divided by distance to dataset %d" % (i))
        temp = df['r'] ** 2
        df['t_delta'] **= 2
        df['t_delta'] = np.sqrt(
            (df['t_delta'] + df['t'] ** 2 * df['r_delta'] / temp) / temp)
        df.loc[:, 't'] = df['t'].values / df['r'].values
        new_dataset[i] = df
    print(SEP)
    return new_dataset
