# from subprocess import Popen, PIPE
import numpy as np
import math
import pandas as pd
from datetime import date; today = date.today().strftime("%d/%m/%y")
from matplotlib.table import Table
from matplotlib.patches import Rectangle

#Sizes of pictures
SIZE_X = {1 : 4.921259842519685, 2 : 6.299212598425196, 'many' : 6.299212598425196,}
SIZE_Y = {1 : 3.149606299212598, 2 : 4.724409448818897, 'many' : 7.874015748031496}

STD_COLS = ['r', 'r_delta', 'rise_time_corrected', 'rise_time_corrected_delta', 'zenith', 'energy',
        'vem_signal', 'high_gain_saturated', 'event_id']
STD_COLS_NAMES = {'rise_time_corrected' : 't', 'rise_time_corrected_delta' : 't_delta', 'zenith' : 'zen', 'zenith_delta' : 'zen_delta',
        'energy' : 'en', 'energy_delta' : 'en_delta', 'vem_signal' : 'vem', 'high_gain_saturated' : 'sat', 'auger_rise_time' : 't'}
# ALL_COLS = ({'r' : 0, 'r_delta' : 1, 't' : 2, 't_delta' : 3, 'zen' : 4, 'zen_delta' : 5, 'en' : 6, 'en_delta' : 7,
        # 'vem' : 8, 'vem_delta' : 9, 'sat' : 10, 'azimuth' : 11, 'eid' : 12, 'rise_time' : 13, 'auger_rise_time' : 14,
        # 'station_id' : 15}, list(range(16)))
# STD = ({'r' : 0, 'r_delta' : 1, 't' : 2, 't_delta' : 3, 'zen' : 4, 'zen_delta' : 5,
    # 'en' : 6, 'en_delta' : 7, 'vem' : 8, 'sat' : 9},
        # [0, 1, 2, 3, 4, 5, 6, 7, 8, 10])
# ARRAY_TRANS = str.maketrans('', '', '[],array()')

pol1 = lambda x, a, b: a + b * x
sec = lambda x: 1 / np.cos(x)
msec = lambda x: 1 / math.cos(x)
asec = lambda x: np.arccos(1 / x)

def std_table(data, ax, pos, title = None):
    l, b, w, h = pos
    entries = len(data)
    mean = data.mean()
    rms = data.std()
    vals = [entries, mean, rms]
    fmt='{:.2f}'
    tb = Table(ax, bbox = pos, zorder = 3)
    width, height = 1, 1.0 / 3
    for i, val in enumerate(vals):
        tb.add_cell(i, 1, width, height, text=fmt.format(val) if i else str(entries), 
                loc='right', facecolor = 'w')
        tb.add_cell(i, 0, width, height,
                text=['Entries', 'Mean', 'RMS'][i], loc='left')
    tb.get_celld()[(0, 1)].visible_edges = 'TR'
    tb.get_celld()[(1, 1)].visible_edges = 'R'
    tb.get_celld()[(2, 1)].visible_edges = 'RB'
    tb.get_celld()[(0, 0)].visible_edges = 'TL'
    tb.get_celld()[(1, 0)].visible_edges = 'L'
    tb.get_celld()[(2, 0)].visible_edges = 'LB'
# tb.get_celld()[(-1, -1)].visible_edges = 'LTR'
# tb.get_celld()[(-1, -1)].set_bounds(0, 0, 2, 2)
    if title:
        title_box = ax.add_patch(Rectangle((l, b + h), w, h / 3, Fill = False, transform = ax.transAxes, zorder = 3, facecolor = 'w'))
        ax.text(l + w / 2, b + 1.1 * h, title,
            transform = ax.transAxes, horizontalalignment = 'center',
            weight = 'bold', fontsize = 'x-small')
        
    return tb

def standard_plot(x, y, z, fun, points, popt, pcov, fig, ax, draw_profile = False):
    ax.clear()
    ax.errorbar(x, y, yerr = z, fmt = 'ob', lw = 0.5)
    if draw_profile:
        xp, yp, zp = profile([x, y])
        ax.errorbar(xp, yp, yerr = zp, fmt = 'sr')
    aux = np.linspace(x.min(), x.max(), points)
    ax.plot(aux, fun(aux, *popt), 'k', zorder = 3)
    ax.text(0.3, 0.8, 'Entries$\,=' + str(len(x)) + '$\n' + 
        '\n'.join(['p' + str(i) + '$\,=%.2f+-%.2f$' %(popt[i], math.sqrt(pcov[i, i]))
            for i in range(len(popt))])
            + '\n' + '$\chi^2/dof\,={:.2f}$'.format(chi_dof(y, fun(x, *popt), z, len(popt))),
            transform = ax.transAxes, bbox = dict(fc = 'w', alpha = 0.5))
    fig.canvas.draw()

def smart_bins(a, bins_num):
    """Return the bin edges of bins_num
    bins such that they all have the same entries.

    Parameters
    ---------- 
    a : ndarray
        Array that will be binned.
    bins_num : int
        Number of bins that will be done.

    Returns
    ---------- 
    bins : ndarray
       The edges of the bins.

    """
    entries = len(a) // bins_num
    percs = np.arange(0, 100 + (100 / bins_num) * 0.999999999999, 100 / bins_num)
    percs[-1] = 100
    bins = np.percentile(a, percs)
    bins[-1] *= 1.00000000001 
    return bins

def chi_square(y, yexp, y_delta):
    return np.sum( ((y - yexp) / y_delta) ** 2)

def chi_dof(y, yexp, y_delta, ddof = 2):
    return chi_square(y, yexp, y_delta) / (len(y) - ddof)

def header_fun(path, args, header):
    if header:
        from inspect import getframeinfo, stack
        caller = getframeinfo(stack()[1][0])
        header += ' Date: ' + today + '. Called from the file "' + caller.filename + '" at line ' + str(caller.lineno) + '.'
        with open(path, 'w') as f:
            f.write(header + '\n')
        args.to_csv(path, sep = ' ', header = False, index = False, mode = 'a')  
    else:
        args.to_csv(path, sep = ' ', header = False, index = False, mode = 'w') 

def to_csv(path, args, header = False, space = True):
    """
    Save as a column separated value the content of args in the file
    given by path. args can be a single DataFrame, or a list with each
    element being the array to save. If given, the header will be printed,
    once for multi-array args.
    space controls whether a blank line is between the multi-arrays or not
    """
    if type(args) == np.ndarray:
        df = pd.DataFrame(args)
        header_fun(path, df, header)
    elif type(args) == pd.DataFrame:
        header_fun(path, args, header)
    elif type(args) == list or type(args) == tuple:
        pd.set_option('max_rows', None)
        df = pd.DataFrame(args[0])
        # print(df)
        # input()
        header_fun(path, df, header)
        for elem in args[1 : ]:
            df = pd.DataFrame(elem)
            if space:
                header = len(df.columns) * ['']
            df.to_csv(path, sep = ' ', header = header, index = False, mode = 'a')
        pd.reset_option('max_rows')
    else:
        raise TypeError("Unknown format for arguments to save")

def profile(data, bins = None, counts = False):
    df = (pd.DataFrame(np.array(data).T, columns = ['x', 'y'])).sort_values(by = 'x', inplace = False).reset_index(
                drop = True)
    if bins is None:
        count, bins = np.histogram(df['x'], 'auto')
        # print("Bins:")
        # print(bins)
    else:
        if type(bins) is list or type(bins) is tuple or\
            type(bins) is np.ndarray:
            df = df.loc[(df['x'] >= bins.min()) & (df['x'] < bins.max())]
        count, bins = np.histogram(df['x'], bins = bins)

    x = (bins[1 : ] + bins[ : -1]) / 2
    tmp = count > 0
    x = x[tmp]
    count = count[tmp]
    bins[-1] *= 1.0000000001
    y = np.empty(x.size)
    y2 = np.empty(x.size)
    counts_vec = np.empty(x.size, dtype = np.int)
    grouped = df.groupby(np.digitize(df['x'], bins))
    for i, (name, group) in enumerate(grouped):
        counts_vec[i] = len(group)
        y[i] = group['y'].mean()
        y2[i] = group['y'].sem(ddof = 0) 
    if counts:
        return [x, y, y2, counts_vec]
    else:
        return [x, y, y2]

s = """18.5466   1.09897   0.0162002   0.392803
18.6467   1.24126   0.0202698   0.39116
18.7474   1.49019   0.0255189   0.389521
18.8483   1.63004   0.0306266   0.387892
18.9487   1.76837   0.0374637   0.386285
19.0476   1.91114   0.0434403   0.384715
19.1471   2.02741   0.0532371   0.383149
19.2472   2.09677   0.0687103   0.381585
19.3466   2.1546   0.0912372   0.380045
19.4457   2.6297   0.111511   0.378523
19.5467   2.33844   0.142896   0.376983
19.6413   2.9331   0.13353   0.375552
19.7424   2.53473   0.192937   0.374035
19.8772   2.30993   0.213085   0.372031"""
# NaN NaN NaN NaN"""

patricia_qgsjet = pd.DataFrame(np.array(s.split(), dtype = np.float).reshape(-1, 4), columns = ['x', 'y', 'stat', 'syst'], dtype = np.float)

s = """18.5466   1.84668   0.0178586   0.433015
18.6467   2.03648   0.0225276   0.43473
18.7474   2.3488   0.0285946   0.436469
18.8483   2.54282   0.0346007   0.438225
18.9487   2.73782   0.042672   0.439987
19.0476   2.94019   0.0498789   0.441737
19.1471   3.11509   0.0616242   0.443511
19.2472   3.23803   0.080185   0.44531
19.3466   3.34874   0.107338   0.447111
19.4457   3.95566   0.13225   0.448922
19.5467   3.65668   0.17087   0.450782
19.6413   4.41709   0.160903   0.452538
19.7424   3.98626   0.234407   0.454431
19.8772   3.77638   0.261739   0.456978"""
# NaN NaN NaN NaN"""
patricia_epos = pd.DataFrame(np.array(s.split(), dtype = np.float).reshape(-1, 4), columns = ['x', 'y', 'stat', 'syst'], dtype = np.float)

#def error(array, index):
#    c = 104.16666666666667
#    theta = array[col['zenith']][index]
#    r = array[col['r']][index] 
#    signal = array[col['vem_signal']][index]
#    print(theta, r, signal)
#    input()
#    if r <= 650:
#        return math.sqrt( ((-340 + 186 / math.cos(theta) + (0.94 - 0.44 / math.cos(theta)) * r)
#            / math.sqrt(signal)) ** 2 + c)
#    else:
#        return math.sqrt( ((-447 + 224 / math.cos(theta) + (1.12 - 0.51 / math.cos(theta)) * r)
#            / math.sqrt(signal)) ** 2 + c)
