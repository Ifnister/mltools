# coding: future_fstrings
import pandas as pd
import math

def read(datafiles, columns=None, verbose=0):
    """
    :param datafiles: Iterable with the paths of the files to read
    Can be a list of strings or a list of a list of strings, in which case
    the datasets that belong to the same primary list are concatenated together
    :params verbose: 0 for no output and 1 for printing which files are being read
    
    :returns: A list with DataFrames
    """
    dataset = []
    for i in range(len(datafiles)):
        if verbose > 0:
            print(f"Reading file {datafiles[i]}")
        if isinstance(datafiles[i], list):
            df = pd.concat([pd.read_hdf(name, header=None) if name[-3:] == 'hdf'
                            else pd.read_csv(name, header=None) for name in
                            datafiles[i]], axis=0, join='inner')
        else:
            df = pd.read_hdf(datafiles[i], header=None) if datafiles[i][-3:] == 'hdf' else pd.read_csv(datafiles[i], header=None)
        try:
            df.rename(columns[i], inplace=True)
        except TypeError:
            pass
        dataset.append(df)
        if verbose > 0:
            print(f"The columns used are: {list(dataset[i].columns)}")
    return dataset
